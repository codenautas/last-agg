<!--multilang v0 es:LEEME.md en:README.md -->
# last-agg

<!--lang:es-->
obtiene el último valor no nulo en una función de agregado postgres
<!--lang:en--]
last non null data agreggate function for postgres

[!--lang:*-->

<!--multilang buttons-->

idioma: ![castellano](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-es.png)
también disponible en:
[![inglés](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-en.png)](README.md)

<!--lang:es-->
# Caso de uso

Se desea repetir el valor de un campo utilizando el valor de arriba si lo hay

<!--lang:en--]
# Case of use

You want to repeat the value of some field if the next row has a `null`.

[!--lang:*-->

id |country   | city 
---|----------|------
 1 |          | Base Antártica
 2 |Argentina | Buenos Aires
 3 |          | Córdoba
 4 |          | Rosario
 5 |Uruguay   | Montevideo


```sql
select id, last_agg(country) over (order by id), city
   from cities
   order by id;
```

id |country   | city 
---|----------|------
 1 |          | Base Antártica
 2 |Argentina | Buenos Aires
 3 |Argentina | Córdoba
 4 |Argentina | Rosario
 5 |Uruguay   | Montevideo

<!--lang:es-->
# Instalación

<!--lang:en--]
# Install

[!--lang:*-->

```sh
psql < bin/create_last_agg.sql
```

<!--lang:es-->

## Licencia

<!--lang:en--]

## License

[!--lang:*-->

[MIT](LICENSE)

