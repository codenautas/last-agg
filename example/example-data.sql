create table "cities" (
 "id" integer,
 "country" character varying(9),
 "city" character varying(14)
);

insert into "cities" ("id","country", "city") values
 (1,null       , 'Base Antártica'),
 (2,'Argentina', 'Buenos Aires'  ),
 (3,null       , 'Córdoba'       ),
 (4,null       , 'Mendoza'       ),
 (5,'Uruguay'  , 'Montevideo'    );

select id, last_agg(country) over (order by city), city
   from cities;