"use strict";

var fs = require('mz/fs');
var pg = require('pg');
var MiniTools = require('mini-tools');

function showAndThrow(msg){
    return function(err){
        console.log('--------',msg);
        console.log(err);
        throw err;
    }
}

describe("last-agg", function(){
    var db;
    var finallyFun=function(){};
    before(()=>
        MiniTools.readConfig([
            'def-config.yaml',
            'local-config.yaml',
        ],{whenNotExist:'ignore'}).catch(function(err){
            console.log('problem reading def-config.yaml or local-config.yaml');
            throw err;
        }).then(config=>{
            console.log('begin testing in',config.db.database);
            finallyFun = () => db.query("SET SEARCH_PATH = "+config.db.schema);
            db = new pg.Client(config.db);
            return db.connect().then(()=>
                db.query("DROP SCHEMA IF EXISTS "+config.db.schema+" CASCADE")
            ).then(()=>
                db.query("CREATE SCHEMA "+config.db.schema)
            ).then(finallyFun);
        }).catch(showAndThrow(1))
    );
    after(function(){
        db.end();
    });
    it("test-all", ()=>
        fs.readFile("last_agg.sql",{encoding:'utf8'}).then(
            sql=>db.query(sql)
        ).catch(showAndThrow(2))
    );
    it("copy to bin", ()=>
        fs.readFile("last_agg.sql",{encoding:'utf8'}).then(
            sql=>fs.writeFile("bin/create_last_agg.sql",sql.split('--TEST')[0])
        ).catch(showAndThrow(3))
    );
})