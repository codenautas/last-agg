# last-agg

last non null data agreggate function for postgres



language: ![English](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-en.png)
also available in:
[![Spanish](https://raw.githubusercontent.com/codenautas/multilang/master/img/lang-es.png)](LEEME.md)

# Case of use

You want to repeat the value of some field if the next row has a `null`.


id |country   | city
---|----------|------
 1 |          | Base Antártica
 2 |Argentina | Buenos Aires
 3 |          | Córdoba
 4 |          | Rosario
 5 |Uruguay   | Montevideo


```sql
select id, last_agg(country) over (order by id), city
   from cities
   order by id;
```

id |country   | city
---|----------|------
 1 |          | Base Antártica
 2 |Argentina | Buenos Aires
 3 |Argentina | Córdoba
 4 |Argentina | Rosario
 5 |Uruguay   | Montevideo

# Install


```sh
psql < bin/create_last_agg.sql
```


## License


[MIT](LICENSE)

