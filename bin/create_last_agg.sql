CREATE OR REPLACE FUNCTION last_agg_agg(p1 anyelement, p2 anyelement) RETURNS anyelement
  LANGUAGE SQL
AS
$SQL$
  SELECT coalesce(p2 ,p1);
$SQL$;

DROP AGGREGATE IF EXISTS last_agg(anyelement);

CREATE AGGREGATE last_agg(anyelement) (
    sfunc = last_agg_agg,
    stype = anyelement
);

