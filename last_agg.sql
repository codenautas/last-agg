CREATE OR REPLACE FUNCTION last_agg_agg(p1 anyelement, p2 anyelement) RETURNS anyelement
  LANGUAGE SQL
AS
$SQL$
  SELECT coalesce(p2 ,p1);
$SQL$;

DROP AGGREGATE IF EXISTS last_agg(anyelement);

CREATE AGGREGATE last_agg(anyelement) (
    sfunc = last_agg_agg,
    stype = anyelement
);

--TEST

do language plpgsql 
$sqls$
declare
  v_clean_test boolean:=true;
  v_int integer;
  v_text text;
  v_date date;
  v_other text;
begin
  -- prepare fixture
  DROP TABLE IF EXISTS test_table_last_agg;
  CREATE TABLE test_table_last_agg(
    an_int integer,
    a_text text,
    a_date date,
    other_text text
  );
  INSERT INTO test_table_last_agg (an_int, a_text, a_date) values
    (1, null, '2017-08-01'),
    (2, 'hi', null        ),
    (3, null, null        ),
    (4, 'ah', null        );
  select last_agg(a_text order by an_int),last_agg(a_date) into v_text, v_date
      from test_table_last_agg;
  -- test
  if v_text is distinct from 'ah' then
    raise 'bad v_text %', v_text;
  end if;
  if v_date is distinct from '2017-08-01'::date then
    raise 'bad v_date %', v_date;
  end if;
  select last_agg(a_text order by an_int),last_agg(a_date) into  v_text, v_date
      from test_table_last_agg
      where an_int in (1,3);
  if v_text is distinct from null then
    raise 'bad null in v_text %', v_text;
  end if;
  -- clean fixture
  if v_clean_test then
    DROP TABLE IF EXISTS test_table_last_agg;
  end if;
end;
$sqls$;

